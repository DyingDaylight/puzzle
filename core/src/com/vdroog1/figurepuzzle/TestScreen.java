package com.vdroog1.figurepuzzle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.vdroog1.figurepuzzle.data.Shape;
import com.vdroog1.figurepuzzle.processor.FigureProcessor;

/**
 * Created by kettricken on 01.03.2015.
 */
public class TestScreen extends ScreenAdapter {

    private PuzzleGame game;
    private OrthographicCamera camera;
    private Viewport viewport;

    FigureProcessor figureProcessor;

    ShapeRenderer shapeRenderer = new ShapeRenderer();

    public TestScreen(PuzzleGame game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(true, Constants.WORLD_WIDTH_M, Constants.WORLD_HEIGHT_M);
        viewport = new StretchViewport(Constants.WORLD_WIDTH_M, Constants.WORLD_WIDTH_M, camera);

        Gdx.input.setInputProcessor(new InputListener());

        figureProcessor = new FigureProcessor();
        figureProcessor.initLevel(Shape.SQUARE);
    }


    @Override
    public void render(float delta) {
        super.render(delta);

        camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);

        figureProcessor.drawDebug(shapeRenderer);
    }

    private class InputListener extends InputAdapter {

        @Override
        public boolean keyDown(int keycode) {
            switch (keycode){
                case Input.Keys.Q:
                    figureProcessor.switchDrawShape();
                    break;
                case Input.Keys.W:
                    figureProcessor.deleteRandomLines();
                    break;
                case Input.Keys.E:
                    figureProcessor.applyShapeMask();
                    break;
                case Input.Keys.R:
                    figureProcessor.getFigures();
                    break;
                case Input.Keys.LEFT:
                    figureProcessor.decrementIndex();
                    break;
                case Input.Keys.RIGHT:
                    figureProcessor.incrementIndex();
                    break;
            }
            return super.keyDown(keycode);
        }
    }

}
