package com.vdroog1.figurepuzzle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.vdroog1.figurepuzzle.model.MovingData;
import com.vdroog1.figurepuzzle.processor.FigureProcessor;

import java.util.Iterator;

/**
 * Created by kettricken on 08.02.2015.
 */
public class GameScreen extends ScreenAdapter {

    public static final float HALF_WALL_WIDTH = 2.5f;
    public static final int MIN_DISTANCE = 6;
    public static final int COEFF = 10;
    private static final float TIME = 60f;
    public static final int ROTATION_SPEED = 1;

    final short CATEGORY_OBJECT = 0x0001;
    final short CATEGORY_GROUND = 0x0002;

    final short MASK_OBJECT = CATEGORY_OBJECT;
    final short MASK_GROUND = 0;

    private PuzzleGame game;
    private World world;
    private OrthographicCamera camera;
    private Viewport viewport;

    private Box2DDebugRenderer debugRenderer;

    private float accumulator = 0;

    boolean isExpanding = false;

    int rotationDirection = 1;

    FigureProcessor figureProcessor;

    //-----
    Array<Body> bodies = new Array<Body>();
    ObjectMap<Body, MovingData> movingObjects = new ObjectMap<Body, MovingData>();
    ObjectMap<Body, Float> lastAngles = new ObjectMap<Body, Float>();
    Body groundBody;
    Body clickedBody;
    Array<Body> bodiesToDelete = new Array<Body>();
    //----

    public GameScreen(PuzzleGame game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(true, Constants.WORLD_WIDTH_M, Constants.WORLD_HEIGHT_M);
        viewport = new StretchViewport(Constants.WORLD_WIDTH_M, Constants.WORLD_WIDTH_M, camera);

        Box2D.init();
        world = new World(new Vector2(0, 0), true);
        debugRenderer = new Box2DDebugRenderer();

        for (int i = 0; i < 2; i++) {
            Body body = createBody(i);
            bodies.add(body);
        }
        createWalls();

        InputMultiplexer inputMultiplexer = new InputMultiplexer(new GestureDetector(new InputProcessor()));
        inputMultiplexer.addProcessor(new DirectionGestureDetector(new DirectionGestureDetector.DirectionGestureListener(new GestureListener()), camera));

        Gdx.input.setInputProcessor(inputMultiplexer);

        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                bodiesToDelete.add(contact.getFixtureA().getBody());
                bodiesToDelete.add(contact.getFixtureB().getBody());
            }

            @Override
            public void endContact(Contact contact) {
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }
        });

        figureProcessor = new FigureProcessor();
        figureProcessor.initLevel(com.vdroog1.figurepuzzle.data.Shape.SQUARE);
    }

    public void deleteDeadBodies() {
        Iterator<Body> bodyIterator = bodiesToDelete.iterator();
        while (bodyIterator.hasNext()){
            Body body = bodyIterator.next();
            if (movingObjects.containsKey(body)) {
                if (movingObjects.get(body).getMouseJoint() != null) {
                    world.destroyJoint(movingObjects.get(body).getMouseJoint());
                }
                movingObjects.remove(body);
            }
            body.setLinearVelocity(0, 0);
            body.applyLinearImpulse(new Vector2(0, 0), body.getPosition(), true);
            bodyIterator.remove();

        }
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        updateBodies(delta);

        camera.update();
        debugRenderer.render(world, camera.combined);
        doPhysicsStep(delta);
        deleteDeadBodies();
    }

    private void updateBodies(float delta) {

        world.getBodies(bodies);
        Iterator<Body> bodyIterator = bodies.iterator();
        while (bodyIterator.hasNext()){
            Body body = bodyIterator.next();
            if (!movingObjects.containsKey(body)) {
                body.setLinearVelocity(0,0);
            }
        }
        if (movingObjects.size == 0 && isExpanding)
            isExpanding = false;

        ObjectMap.Entries<Body, MovingData> entries = movingObjects.iterator();
        while (entries.hasNext()) {
            ObjectMap.Entry<Body, MovingData> entry = entries.next();

            if (entry.value.getMouseJoint() == null)
                continue;

            Vector2 position = entry.key.getPosition();
            Vector2 destination = entry.value.getDestination();
            Vector2 start = clickedBody.getPosition(); //center
            float speed = entry.value.getSpeed();

            float desiredDif = (float) Math.sqrt(Math.pow(destination.x - start.x, 2) + Math.pow(destination.y - start.y, 2));
            float realDif = (float) Math.sqrt(Math.pow(position.x - start.x, 2) + Math.pow(position.y - start.y, 2)); //radius
            if (realDif < desiredDif && !entry.value.isMovingBack()) {
                float diff = (float) Math.sqrt(Math.pow(position.x - destination.x, 2) + Math.pow(position.y - destination.y, 2));
                entry.value.setMovingBack(false);
                position.x = position.x + speed / diff * (destination.x - position.x);
                position.y = position.y + speed / diff * (destination.y - position.y);
                entry.value.getMouseJoint().setTarget(position);
            } else if (entry.value.getHangingTime() < TIME) {
                if (!entry.value.isMovingBack()) {
                    entry.value.setMovingBack(true);
                }
                entry.value.increaseHangingTime(delta);

                float angle = MathUtils.atan2(position.y - start.y, position.x - start.x) * 180 / MathUtils.PI;
                if (!lastAngles.containsKey(entry.key)) {
                    lastAngles.put(entry.key, angle);
                }
                float difAn = (angle - lastAngles.get(entry.key));
                lastAngles.put(entry.key, angle);
                angle = angle + ROTATION_SPEED * rotationDirection;

                float newAngle = (entry.key.getAngle() /  MathUtils.degRad + difAn) * MathUtils.degRad;
                entry.key.setTransform(entry.key.getPosition(), newAngle);

                position.x = start.x + realDif * (float)Math.cos(angle * MathUtils.degRad);
                position.y = start.y + realDif * (float)Math.sin(angle * MathUtils.degRad);
                entry.value.getMouseJoint().setTarget(position);
            } else  {
                if (!entry.value.isMovingBack()) {
                    entry.value.setMovingBack(true);
                }
                float diff = (float) Math.sqrt(Math.pow(position.x - start.x, 2) + Math.pow(position.y - start.y, 2));
                if (diff != 0) {
                    position.x = position.x + speed / diff * (start.x - position.x);
                    position.y = position.y + speed / diff * (start.y - position.y);
                    entry.value.getMouseJoint().setTarget(position);
                } else {
                    bodiesToDelete.add(entry.key);
                }
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    private void doPhysicsStep(float delta) {
        float frameTime = Math.min(delta, 0.25f);
        accumulator += frameTime;
        while (accumulator >= Constants.TIME_STEP) {
            world.step(Constants.TIME_STEP, Constants.VELOCITY_ITERATIONS,
                    Constants.POSITION_ITERATIONS);
            accumulator -= Constants.TIME_STEP;
        }

    }

    private void createWalls() {
        BodyDef groundBodyDef = new BodyDef();
        groundBodyDef.position.set(new Vector2(Constants.WORLD_WIDTH_M / 2, HALF_WALL_WIDTH));

        PolygonShape groundBox = new PolygonShape();
        groundBox.setAsBox(Constants.WORLD_WIDTH_M / 2, HALF_WALL_WIDTH);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.filter.categoryBits = CATEGORY_GROUND;
        fixtureDef.filter.maskBits = MASK_GROUND;
        fixtureDef.shape = groundBox;
        fixtureDef.density = 0f;

        groundBody = world.createBody(groundBodyDef);
        groundBody.createFixture(fixtureDef);

        groundBox.dispose();
    }

    private Body createBody(int i) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        switch (i) {
            case 0:
                bodyDef.position.set(50, 30);
                break;
            case 1:
                bodyDef.position.set(50, 40);
                break;
            case 2:
                bodyDef.position.set(50, 20);
                break;
            case 3:
                bodyDef.position.set(40, 30);
                break;
            case 4:
                bodyDef.position.set(60, 30);
                break;
        }


        Body body = world.createBody(bodyDef);

        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(5f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.restitution = 0f;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0f;
        fixtureDef.filter.categoryBits = CATEGORY_OBJECT;
        fixtureDef.filter.maskBits = MASK_OBJECT;

        Fixture fixture = body.createFixture(fixtureDef);
        circleShape.dispose();

        return body;
    }

    private class InputListener extends InputAdapter {

    }

    private class GestureListener implements DirectionGestureDetector.DirectionListener {

        @Override
        public void onLeft() {
            Gdx.app.log("Test", "left");
            rotationDirection = -1;
        }

        @Override
        public void onRight() {
            Gdx.app.log("Test", "right");
            rotationDirection = 1;
        }

        @Override
        public void onUp() {
            Gdx.app.log("Test", "up");
            rotationDirection = -1;
        }

        @Override
        public void onDown() {
            Gdx.app.log("Test", "down");
            rotationDirection = 1;
        }

        @Override
        public void clockwise() {
            Gdx.app.log("Test", "clockwise");
            rotationDirection = -1;
        }

        @Override
        public void conterClockwise() {
            Gdx.app.log("Test", "conterClockwise");
            rotationDirection = 1;
        }
    }

    private class InputProcessor extends GestureDetector.GestureAdapter {

        Vector3 touchPoint = new Vector3();

        @Override
        public boolean longPress(float x, float y) {
            world.getBodies(bodies);
            for (Body worldBody : bodies) {
                worldBody.setTransform(worldBody.getPosition(), 90f * MathUtils.degRad);
                Gdx.app.log("Test", "angle " + (worldBody.getAngle() /  MathUtils.degRad));
            }
            return super.longPress(x, y);

        }



        @Override
        public boolean tap(float screenX, float screenY, int count, int button) {
            if (isExpanding) {
                world.getBodies(bodies);
                for (Body worldBody : bodies) {
                    if (movingObjects != null && movingObjects.containsKey(worldBody)) {
                        movingObjects.get(worldBody).increaseHangingTime(TIME);
                    }
                }
            }

            touchPoint.set(screenX, screenY, 0);
            camera.unproject(touchPoint);
            world.QueryAABB(new QueryCallback() {
                @Override
                public boolean reportFixture(Fixture fixture) {
                    Body body = fixture.getBody();
                    if (!isExpanding) {
                        Array<Contact> contacts = world.getContactList();
                        for (Contact contact : contacts) {
                            if (contact.getFixtureA().getBody().equals(body) ||
                                    contact.getFixtureB().getBody().equals(body)) {
                                setBodyMoving(body);
                                break;
                            }
                        }
                    }
                    return false;
                }
            }, touchPoint.x - 0.1f, touchPoint.y - 0.1f, touchPoint.x + 0.1f, touchPoint.y + 0.1f);

            return false;
        }
    }

    private void setBodyMoving(Body body) {
        world.getBodies(bodies);
        for (Body worldBody : bodies) {
            if (!worldBody.equals(body) && worldBody.getType() == BodyDef.BodyType.DynamicBody) {
                MouseJointDef def = new MouseJointDef();
                def.bodyA = groundBody;
                def.bodyB = worldBody;
                def.collideConnected = true;
                def.target.set(worldBody.getPosition().x, worldBody.getPosition().y);
                def.maxForce = 10000.0f;

                MouseJoint mouseJoint = (MouseJoint) world.createJoint(def);
                worldBody.setAwake(true);
                MovingData movingData = new MovingData();

                Vector2 currentPoint = new Vector2(worldBody.getPosition());
                Vector2 center = body.getPosition();
                float diff = (float) Math.sqrt(Math.pow(currentPoint.x - center.x, 2) + Math.pow(currentPoint.y - center.y, 2));
                float speed = diff / COEFF;
                float moveDist = MIN_DISTANCE * diff / COEFF;
                currentPoint.x = currentPoint.x - moveDist / diff * (center.x - currentPoint.x);
                currentPoint.y = currentPoint.y - moveDist / diff * (center.y - currentPoint.y);

                movingData.setDestinationPoint(currentPoint.x, currentPoint.y);
                movingData.setMouseJoint(mouseJoint);
                movingData.setSpeed(speed);
                movingObjects.put(worldBody, movingData);
                clickedBody = body;
                isExpanding = true;

                float angle = MathUtils.atan2(worldBody.getPosition().y - clickedBody.getPosition().y, worldBody.getPosition().x - clickedBody.getPosition().x) * 180 / MathUtils.PI;
                lastAngles.put(worldBody, angle);
            }
        }
        camera.position.x = body.getPosition().x;
        camera.position.y = body.getPosition().y;
        camera.position.z = 0;
    }

    private Vector2 getCenterPosition(Vector2 position) {
        return new Vector2(position.x, position.y);
    }
}
