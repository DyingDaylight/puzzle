package com.vdroog1.figurepuzzle;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


public class DirectionGestureDetector extends GestureDetector /*implements InputProcessor*/{
    private Vector3 touchPoint = new Vector3();

    public  interface DirectionListener{
        void onLeft();
        void onRight();
        void onUp();
        void onDown();

        void clockwise();
        void conterClockwise();
    }

    DirectionGestureListener listener;
    final OrthographicCamera camera;

    public DirectionGestureDetector(DirectionGestureListener directionListener, OrthographicCamera camera) {
        super(directionListener);
        this.listener = directionListener;
        this.camera = camera;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touchPoint.set(screenX, screenY, 0);
        camera.unproject(touchPoint);

        listener.setTouchPoint(touchPoint.x, touchPoint.y);
        return false;
    }

    public static class  DirectionGestureListener extends GestureAdapter {
        DirectionListener directionListener;

        Vector2 touchPoint = new Vector2();

        public DirectionGestureListener(DirectionListener directionListener){
            this.directionListener = directionListener;
        }

        @Override
        public boolean fling(float velocityX, float velocityY, int button) {
            if(Math.abs(velocityX)>Math.abs(velocityY)){
                if(velocityX>0){
                    if (touchPoint.y <= Constants.WORLD_HEIGHT_M / 2)
                        directionListener.conterClockwise();
                    else
                        directionListener.clockwise();
                }else{
                    if (touchPoint.y <= Constants.WORLD_HEIGHT_M / 2)
                        directionListener.clockwise();
                    else
                        directionListener.conterClockwise();

                }
            }else{
                if(velocityY>0){
                    if (touchPoint.x <= Constants.WORLD_WIDTH_M / 2)
                        directionListener.conterClockwise();
                    else
                        directionListener.clockwise();
                }else{
                    if (touchPoint.x <= Constants.WORLD_WIDTH_M / 2)
                        directionListener.clockwise();
                    else
                        directionListener.conterClockwise();
                }
            }
            return super.fling(velocityX, velocityY, button);
        }

        public void setTouchPoint(float x, float y) {
            touchPoint.set(x, y);
        }
    }
}
