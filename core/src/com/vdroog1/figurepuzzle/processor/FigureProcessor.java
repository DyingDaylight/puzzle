package com.vdroog1.figurepuzzle.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.vdroog1.figurepuzzle.Constants;
import com.vdroog1.figurepuzzle.data.Shape;
import com.vdroog1.figurepuzzle.data.Sides;
import com.vdroog1.figurepuzzle.model.Figure;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by kettricken on 01.03.2015.
 */
public class FigureProcessor {

    private int size = 5;
    private float halfSize = size / 2f;

    private int seed = 100;
    private int minLines = 1;
    private int maxLines = 6;
    private int minBit = 0;
    private int maxBit = 7;

    private float offsetX = 0;
    private float offsetY = 0;

    //TODO: make it work
    private int deletionIterations = 1;

    private int[][] field;
    private int[][] mainFigure;
    private Shape shape;

    boolean drawShape = true;
    boolean isMaskApplied = false;
    boolean drawFirstPoly = false;
    int index = 0;

    private Array<Figure> figureList = new Array<Figure>();

    public FigureProcessor() {

    }

    public void initLevel(Shape shape) {
        long start = System.currentTimeMillis();
        initShape(shape);
        initField();
        initFigure();
        calculateOffset();

        for (int i = 0; i < deletionIterations; i++) {
            deleteRandomLines();
        }
        applyShapeMask();
        getFigures();

        long time = System.currentTimeMillis() - start;
        Gdx.app.log("Test", "initLevel time " + time);
    }

    private void initShape(Shape shape) {
        this.shape = shape;
    }

    private void initField() {
        if (shape == null)
            throw new IllegalAccessError("initField: Shape has not been chosen!");

        field = new int[shape.shape.length][shape.shape[0].length];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                field[i][j] = 255;
            }
        }
    }

    private void initFigure() {
        mainFigure = new int[shape.shape.length][shape.shape[0].length];
    }

    private void calculateOffset() {
        if (field == null)
            throw new IllegalAccessError("calculateOffset: Field has not been initialized!");

        offsetX = Constants.WORLD_WIDTH_M / 2 - field.length * 5 / 2;
        offsetY = Constants.WORLD_HEIGHT_M / 2 - field.length * 5 / 2;
    }

    public void switchDrawShape() {
        drawShape = !drawShape;
    }

    // --- Methods ---

    public void decrementIndex() {
        if (figureList.size == 0)
            return;

        if (index > 0)
            index--;
    }

    public void incrementIndex() {
        if (figureList.size == 0)
            return;

        if (index < figureList.size - 1)
            index++;
    }

    public void deleteRandomLines() {
        if (field == null)
            throw new IllegalAccessError("deleteRandomLines: Field has not been initialized!");

        Random rand = new Random(seed);
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                int linesNumDelete = rand.nextInt((maxLines - minLines) + 1) + minLines;

                for (int k = 0; k < linesNumDelete; k++) {
                    int bitToDelete = rand.nextInt((maxBit - minBit) + 1) + minBit;

                    boolean canDeleteSide = canDeleteSide(j, i, bitToDelete);
                    if (!canDeleteSide)
                        continue;

                    int sideToDelete = (int) Math.pow(2, bitToDelete);

                    int neighbourBitToDelete = (int) (Math.log(Sides.oppositeSide.get(sideToDelete))/Math.log(2));
                    int neighbourColumn = (int) (j + Sides.oppositeCells.get(sideToDelete).x);
                    int neighbourRow = (int) (i + Sides.oppositeCells.get(sideToDelete).y);

                    boolean canDeleteNeighbour = canDeleteSide(neighbourColumn, neighbourRow, neighbourBitToDelete);

                    if (!canDeleteNeighbour)
                        continue;

                    deleteSide(j, i, bitToDelete);
                    deleteSide(neighbourColumn, neighbourRow, neighbourBitToDelete);
                }
            }
        }
    }

    public void applyShapeMask() {
        if (field == null)
            throw new IllegalAccessError("applyShapeMask: Field has not been initialized!");

        if (shape == null)
            throw new IllegalAccessError("applyShapeMask: Shape has not been initialized!");

        if (mainFigure == null)
            throw new IllegalAccessError("applyShapeMask: Figure has not been initialized!");

        isMaskApplied = true;

        for (int i = 0; i < shape.shape.length; i++) {
            for (int j = 0; j < shape.shape[0].length; j++) {
                if (i < 0 || i >= field.length || j < 0 || j >= field[0].length)
                    continue;

                mainFigure[i][j] = field[i][j] & shape.shape[i][j];
                mainFigure[i][j] = mainFigure[i][j] | shape.border[i][j];
            }
        }
    }

    public void getFigures() {
        if (field == null)
            throw new IllegalAccessError("getFigures: Field has not been initialized!");

        if (mainFigure == null)
            throw new IllegalAccessError("getFigures: Figure has not been initialized!");

        for (int i = 0; i < mainFigure.length; i++) {
            for (int j = 0; j < mainFigure[0].length; j++) {
                if (i < 0 || i >= field.length || j < 0 || j >= field[0].length)
                    continue;
                getFigure(j, i);
            }
        }
        drawFirstPoly = true;
    }

    private void getFigure(int column, int row) {
        for (int side : Sides.sidesList) {
            if (hasSide(mainFigure[row][column], side)) {

                Figure figure = new Figure();
                figure.setMatrix(new int[mainFigure.length][mainFigure[0].length]);

                figure.addSideToCell(column, row, side);
                figure.getVertices().add(getCellCenterCoordinates(column, row));
                figure.getVertices().add(getCellSideCoordinates(column, row, side));

                for (int i = 0; i < Sides.correspondingSides.get(side).length; i ++) {
                    int nextColumn = column + (int) Sides.correspondingCells.get(side)[i].x;
                    int nextRow = row + (int) Sides.correspondingCells.get(side)[i].y;

                    if (nextRow < 0 || nextRow >= field.length || nextColumn < 0 || nextColumn >= field[0].length)
                        continue;

                    int correspondSide = Sides.correspondingSides.get(side)[i];

                    if (hasSide(mainFigure[nextRow][ nextColumn], correspondSide)) {
                        figure.addSideToCell(nextColumn, nextRow, correspondSide);
                        figure.getVertices().add(getCellCenterCoordinates(nextColumn, nextRow));

                        int correspondingSideIndex = (int) (Math.log(correspondSide)/Math.log(2));
                        getLine(figure, nextColumn, nextRow, correspondingSideIndex);
                        break;
                    }
                }
                figure.calculateCenter();
                figure.calculateCoordinates();
                figure.calculateIndexes();
                addFigureToList(figure);
            }
        }
    }

    private void getLine(Figure figure, int column, int row, int sideIndex) {
        int currentSideIndex = sideIndex - 1;
        if (currentSideIndex == -1) currentSideIndex = 7;
        boolean allSidesChecked = false;
        while (!allSidesChecked){
            int side = Sides.sidesList[currentSideIndex];
            if (hasSide(mainFigure[row][column], side)) {

                if (hasSide(figure.getMatrix()[row][column], side))
                    return;

                figure.addSideToCell(column, row, side);
                figure.getVertices().add(getCellSideCoordinates(column, row, side));

                for (int i = 0; i < Sides.correspondingCells.get(side).length; i ++) {
                    int nextColumn = column + (int) Sides.correspondingCells.get(side)[i].x;
                    int nextRow = row + (int) Sides.correspondingCells.get(side)[i].y;
                    if (nextRow < 0 || nextRow >= field.length || nextColumn < 0 || nextColumn >= field[0].length)
                        continue;

                    int correspondSide = Sides.correspondingSides.get(side)[i];
                    if (hasSide(mainFigure[nextRow][nextColumn], correspondSide)) {
                        figure.addSideToCell(nextColumn, nextRow, correspondSide);

                        Vector2 center = getCellCenterCoordinates(nextColumn, nextRow);
                        if (!(center.x == figure.getVertices().get(0).x && center.y == figure.getVertices().get(0).y))
                            figure.getVertices().add(getCellCenterCoordinates(nextColumn, nextRow));

                        int nextSideIndex = (int) (Math.log(correspondSide)/Math.log(2));
                        getLine(figure, nextColumn, nextRow, nextSideIndex);
                        return;
                    }
                }
            }

            currentSideIndex--;
            if (currentSideIndex == -1) currentSideIndex = 7;
            if (currentSideIndex == sideIndex)
                allSidesChecked = true;
        }
    }

    private void addFigureToList(Figure figure) {
        for (Figure figureFromList : figureList) {
            int[][] matrix = figureFromList.getMatrix();
            if (Arrays.deepEquals(figure.getMatrix(), matrix))
                return;
        }
        if (!Arrays.deepEquals(figure.getMatrix(), shape.border))
            figureList.add(figure);
    }

    private boolean canDeleteSide(int column, int row, int bitToDelete) {
        if (row < 0 || row >= field.length || column < 0 || column >= field[0].length)
            return true;

        int value = field[row][column];

        int tmpValue = deleteBit(value, bitToDelete);
        return hasTwoOrMoreSides(tmpValue);
    }

    private void deleteSide(int column, int row, int bitToDelete) {
        if (row < 0 || row >= field.length || column < 0 || column >= field[0].length)
            return;

        int value = field[row][column];
        field[row][column] = deleteBit(value, bitToDelete);
    }

    private int deleteBit(int value, int bitToDelete) {
        return value & ~(1 << bitToDelete);
    }

    private boolean hasTwoOrMoreSides(int value) {
        int count = getSidesCount(value);
        return count >= 2;
    }

    private int getSidesCount(int value) {
        int count = 0;
        for (int side : Sides.sidesList) {
            if (hasSide(value, side))
                count++;
        }
        return count;
    }

    private boolean hasSide(int cellValue, int side) {
        int c = cellValue & side;
        return c == side;
    }

    private Vector2 getCellCenterCoordinates(int column, int row) {
        return new Vector2(column * size + halfSize + offsetX,
                row * size + halfSize + offsetY);
    }

    private Vector2 getCellSideCoordinates(int column, int row, int side) {
        return new Vector2(column * size + halfSize + Sides.coordinatesStep.get(side).x * size + offsetX,
                row * size + halfSize + Sides.coordinatesStep.get(side).y * size + offsetY);
    }

    // --- Draw debug ---

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        drawDebugNet(shapeRenderer);

        if (isMaskApplied) {
            shapeRenderer.setColor(0, 1, 0, 1);
            drawMatrix(shapeRenderer, mainFigure);
            if (drawFirstPoly) {
                int[][] matrix = figureList.get(index).getMatrix();
                shapeRenderer.setColor(1, 1, 1, 1);
                drawMatrix(shapeRenderer, matrix);
            }
        } else {
            shapeRenderer.setColor(0, 1, 0, 1);
            drawMatrix(shapeRenderer, field);
        }

        if (drawShape) {
            shapeRenderer.setColor(0, 0, 1, 1);
            drawMatrix(shapeRenderer, shape.shape);
        }

        shapeRenderer.end();
    }

    private void drawDebugNet(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(1, 0, 0, 1);
        for (int i = 0; i < shape.shape.length; i++) {
            for (int j = 0; j < shape.shape[0].length; j++) {
                shapeRenderer.rect((float) (j * size) + offsetX, (float) (i * size) + offsetY, size, size);
            }
        }
    }

    private void drawMatrix(ShapeRenderer shapeRenderer, int[][] matrix) {
        if (matrix == null) {
            Gdx.app.log("Error", "Matrix is not initialized");
            return;
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                for (int side : Sides.sidesList)
                    if (hasSide(matrix[i][j], side))
                        shapeRenderer.line(getCellCenterCoordinates(j, i), getCellSideCoordinates(j, i, side));
            }
        }
    }

    private void printVertices(Array<Vector2> vertices) {
        String verticesStr = "\n[ ";
        for (Vector2 vertex : vertices) {
            verticesStr += "(" + vertex.x + "," + vertex.y + "),";
        }
        verticesStr += " ]";
        Gdx.app.log("FigureProcessorDebug", "Vertices:" + verticesStr);
    }

    private void printIndexes(Array<Integer> indexes) {
        String verticesStr = "\n[ ";
        for (Integer index : indexes) {
            verticesStr += index + ", ";
        }
        verticesStr += " ]";
        Gdx.app.log("FigureProcessorDebug", "Indexes:" + verticesStr);
    }

    // --------------------

}
