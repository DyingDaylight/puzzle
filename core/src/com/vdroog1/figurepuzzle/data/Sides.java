package com.vdroog1.figurepuzzle.data;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by kettricken on 01.03.2015.
 */
public class Sides {

    public static final int TOP = 1;
    public static final int TOP_RIGHT = 2;
    public static final int RIGHT = 4;
    public static final int BOTTOM_RIGHT = 8;
    public static final int BOTTOM = 16;
    public static final int BOTTOM_LEFT = 32;
    public static final int LEFT = 64;
    public static final int TOP_LEFT = 128;

    public static final int[] sidesList = new int[] {
            TOP,
            TOP_RIGHT,
            RIGHT,
            BOTTOM_RIGHT,
            BOTTOM,
            BOTTOM_LEFT,
            LEFT,
            TOP_LEFT
    };

    public static final ObjectMap<Integer, Integer[]> correspondingSides = new ObjectMap<Integer, Integer[]>() {{
        put(TOP, new Integer[]{BOTTOM});
        put(TOP_RIGHT, new Integer[]{TOP_LEFT, BOTTOM_LEFT, BOTTOM_RIGHT});
        put(RIGHT, new Integer[]{LEFT});
        put(BOTTOM_RIGHT, new Integer[]{TOP_RIGHT, TOP_LEFT, BOTTOM_LEFT});
        put(BOTTOM, new Integer[]{TOP});
        put(BOTTOM_LEFT, new Integer[]{BOTTOM_RIGHT, TOP_RIGHT, TOP_LEFT});
        put(LEFT, new Integer[]{RIGHT});
        put(TOP_LEFT, new Integer[]{BOTTOM_LEFT, BOTTOM_RIGHT, TOP_RIGHT});
    }};

    public static final ObjectMap<Integer, Vector2[]> correspondingCells = new ObjectMap<Integer, Vector2[]>() {{
        put(TOP, new Vector2[]{new Vector2(0, -1)});
        put(TOP_RIGHT, new Vector2[]{new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), });
        put(RIGHT, new Vector2[]{new Vector2(1, 0)});
        put(BOTTOM_RIGHT, new Vector2[]{new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0)});
        put(BOTTOM, new Vector2[]{new Vector2(0, 1)});
        put(BOTTOM_LEFT, new Vector2[]{new Vector2(-1, 0), new Vector2(-1, 1), new Vector2(0, 1)});
        put(LEFT, new Vector2[]{new Vector2(-1, 0)});
        put(TOP_LEFT, new Vector2[]{new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0)});
    }};

    public static final ObjectMap<Integer, Integer> oppositeSide = new ObjectMap<Integer, Integer>() {{
        put(TOP,BOTTOM);
        put(TOP_RIGHT, BOTTOM_LEFT);
        put(RIGHT, LEFT);
        put(BOTTOM_RIGHT, TOP_LEFT);
        put(BOTTOM, TOP);
        put(BOTTOM_LEFT, TOP_RIGHT);
        put(LEFT, RIGHT);
        put(TOP_LEFT, BOTTOM_RIGHT);
    }};

    public static final ObjectMap<Integer, Vector2> oppositeCells = new ObjectMap<Integer, Vector2>() {{
        put(TOP, new Vector2(0, -1));
        put(TOP_RIGHT, new Vector2(1, -1));
        put(RIGHT, new Vector2(1, 0));
        put(BOTTOM_RIGHT, new Vector2(1, 1));
        put(BOTTOM, new Vector2(0, 1));
        put(BOTTOM_LEFT, new Vector2(-1, 1));
        put(LEFT, new Vector2(-1, 0));
        put(TOP_LEFT, new Vector2(-1, -1));
    }};

    public static final  ObjectMap<Integer, Vector2> coordinatesStep = new ObjectMap<Integer, Vector2>() {{
        put(TOP, new Vector2(0, -0.5f));
        put(TOP_RIGHT, new Vector2(0.5f, -0.5f));
        put(RIGHT, new Vector2(0.5f, 0));
        put(BOTTOM_RIGHT, new Vector2(0.5f, 0.5f));
        put(BOTTOM, new Vector2(0, 0.5f));
        put(BOTTOM_LEFT, new Vector2(-0.5f, 0.5f));
        put(LEFT, new Vector2(-0.5f, 0f));
        put(TOP_LEFT, new Vector2(-0.5f, -0.5f));
    }};
}
