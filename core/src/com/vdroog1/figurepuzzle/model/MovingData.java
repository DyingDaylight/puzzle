package com.vdroog1.figurepuzzle.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;

public class MovingData {

    Vector2 destinationPoint = new Vector2();

    Vector2 startPoint;

    MouseJoint mouseJoint;

    float speed = 1;
    private boolean isMovingBack = false;
    private float hangingTime;

    public void setDestinationPoint(float x, float y) {
        destinationPoint.set(x, y);
    }


    public void setMouseJoint(MouseJoint mouseJoint) {
        this.mouseJoint = mouseJoint;
    }

    public MouseJoint getMouseJoint() {
        return mouseJoint;
    }

    public Vector2 getDestination() {
        return destinationPoint;
    }

    public float getSpeed() {
        return speed;
    }

    public void setMovingBack(boolean bool) {
        isMovingBack = bool;
    }

    public  boolean isMovingBack() {
        return isMovingBack;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getHangingTime() {
        return hangingTime;
    }

    public void increaseHangingTime(float delta) {
        hangingTime += delta;
    }
}
