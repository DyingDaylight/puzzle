package com.vdroog1.figurepuzzle.model;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by kettricken on 01.03.2015.
 */
public class Figure {

    private Vector2 coordinates = new Vector2(0, 0);
    private Vector2 center = new Vector2(0, 0);

    private int[][] matrix;

    private Polygon polygon = new Polygon();

    private Array<Vector2> vertices = new Array<Vector2>();
    private Array<Integer> indexes = new Array<Integer>();

    public Array<Vector2> getVertices() {
        return vertices;
    }

    public Array<Integer> getIndexes() {
        return indexes;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    public void calculateCenter() {
        float x = 0;
        float y = 0;

        for (Vector2 vertex : vertices) {
            x += vertex.x;
            y += vertex.y;
        }

        center.x = x / vertices.size;
        center.y = y / vertices.size;
    }

    public void calculateCoordinates() {
        if (vertices.size == 0)
            throw new IllegalAccessError("Vertices are not counted yet");

        coordinates.x = vertices.get(0).x;
        coordinates.y = vertices.get(0).y;
    }

    public void calculateIndexes() {
        for (Vector2 vertex : vertices) {
            indexes.add(getVertexIndex(vertex.x, vertex.y));
        }
    }

    private int getVertexIndex(float x, float y) {
        if (matrix == null)
            throw new IllegalAccessError("Matrix is empty. Fill the matrix first");

        int matrixDoubledWidth = matrix.length * 2;
        return  (int) (y * 2 * matrixDoubledWidth + x * 2);
    }


    public void addSideToCell(int column, int row, int side) {
        matrix[row][column] = matrix[row][column] + side;
    }
}
